# 电赛程序库

## 介绍
本仓库用于存放之前参赛调试的程序和电路图或者收集到的各种资料，基本上都通过调试比较好用，程序和电路图虽然有，还是需要大家根据自己的开发板还有具体的模拟器件的型号进行调试，针对自己的器件调试出来最适合自己开发板的程序或者电路，欢迎大家借鉴学习

## 模拟部分资料

模拟部分资料里面存放了一些常用电路的仿真设计，有部分的调试效果不好或者错误请见谅，另外里面包含了电赛综合测评的往年赛题的仿真参考，提供大家学习交流。


## 32开发板、430开发板
主要使用内置的AD DA进行开发用于测量和信号产生部分的设计。
高速模块有使用9854进行驱动DDS产生信号。
提高精度的话就使用FPGA进行测量。
有FPGA和32并行的通信程序，简单的IO操作配置，并行IO

## FPGA部分
这部分主要是用FPGA进行测频测幅，模块通常用高速AD DA进行开发，淘宝很好找 我用过的几款有 9226 9767 7606
FPGA虽然精度高，但是在开发编写进行拟合的时候，相比单片机来说不算方便，所以提供了交互通信的程序，或者也可以自行使用uart等常见的通信协议进行传输数据

## 网盘资料见文章
https://vuko-wxh.blog.csdn.net/article/details/113774142

